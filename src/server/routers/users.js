const router = require("express").Router();
const bcrypt = require("bcrypt");

router.get("/", (req, res) => {
  const db = require("../index");

  db.query(
    "SELECT id, name, user_name, email FROM users",
    (err, result) => {
      console.log(err);
      if (err) res.status(500).json({ error: "Some error while fetching" });
      res.status(200).json(result);
    }
  );
});

router.get("/:id", (req, res) => {
  const db = require("../index");

  db.query(
    `SELECT name,user_name, email FROM users WHERE id = '${req.params.id}'`,
    (err, result) => {
      if (err) res.status(404).json({ error: "User does not exist" });
      if(result.length <= 0) return res.status(401).json({ error: "Wrong user id" });
      res.status(200).json(result);
    }
  );
});

router.post("/", (req, res) => {
  const db = require("../index");
  const body = req.body;
  const salt = bcrypt.genSaltSync();
  const hasedPassword = bcrypt.hashSync(body.password, salt);
  db.query(
    `INSERT INTO users(name, user_name, email, password) VALUES('${body.name}', '${body.user_name}', '${body.email}', '${hasedPassword}')`,
    (err, result) => {
      if (err) res.status(409).json({ error: "User_name already exists" });
      else res.status(201).json({ result: "Success" });
    }
  );
});

router.put("/:id", (req, res) => {
  const id = req.params.id;
  if (!id) return res.status(401).json({ error: "Unauthorized" });
  if (id != req.body.id) return res.status(403).json({ error: "Forbidden" });
  const db = require("../index");
  const name = req.body.name || "";
  const email = req.body.email || "";
  const password = req.body.password || "";

  const salt = bcrypt.genSaltSync();
  const hashedPassword = bcrypt.hashSync(password, salt);

  const updateValue =
    `id = '${id}'` +
    `${name && `, name = '${name}'`}` +
    `${email && `, email = '${email}'`}` +
    `${password && `, password = '${hashedPassword}'`}`;

  db.query(
    `UPDATE users SET '${updateValue}' WHERE id = '${id}'`,
    (err, result) => {
      if (err)
        return res.status(500).json({ error: "Some error while updating" });
      else res.status(200).json(req.body);
    }
  );
});

router.delete("/:id", (req, res) => {
  const id = req.body.id;
  if (!id) return res.status(401).json({ error: "Unauthorized" });
  if (id != req.params.id) return res.status(403).json({ error: "Forbidden" });
  const db = require("../index");

  db.query(`DELETE FROM users WHERE id = '${req.params.id}'`, (err, result) => {
    if (err) res.status(404).json({ error: "User does not exist" });
    else res.status(200).json({ result: "success" });
  });
});

module.exports = router;
