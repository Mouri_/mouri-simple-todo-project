const router = require("express").Router();

router.get("/", (req, res) => {
  const db = require("../index");

  db.query(
    "SELECT subtasks.id, users.name, tasks.id, tasks.title, subtasks.description, subtasks.status FROM subtasks JOIN tasks ON tasks.id = subtasks.task_id JOIN users on users.id = tasks.name_id ORDER BY tasks.id",
    (err, result) => {
      if (err) res.status(500).json({ error: "Some error while fetching" });
      else res.status(200).json(result);
    }
  );
});

router.get("/:id", (req, res) => {
  const db = require("../index");

  db.query(
    `SELECT subtasks.id, users.name, tasks.title, tasks.status FROM subtasks JOIN tasks ON tasks.id = subtasks.task_id JOIN users ON users.id = tasks.name_id WHERE tasks.id = ${req.params.id}`,
    (err, result) => {
      if (err) return res.status(404).json({ error: "User does not exist" });
      if(result.length <= 0) return res.status(401).json({ error: "Wrong task id or no subtasks" });
      else res.status(200).json(result);
    }
  );
});

router.post("/", (req, res) => {
  const db = require("../index");
  const body = req.body;
  db.query(
    `INSERT INTO subtasks(task_id, description) VALUES('${body.task_id}', '${body.description}')`,
    (err, result) => {
      if (err) res.status(409).json({ error: "Subtask already exists or Invalid task_id" });
      else res.status(201).json({ result: "Success" });
    }
  );
});

router.put("/:id", (req, res) => {
  const db = require("../index");
  const description = req.body.description || "";
  const status = req.body.status || "";

  const updateValue =
    `id = '${req.params.id}'` +
    `${description && `, description = '${description}'`}` +
    `${status && `, status = '${status}'`}`;

  db.query(
    `UPDATE subtasks SET ${updateValue} WHERE subtasks.id = ${req.params.id}`,
    (err, result) => {
      if (err) return res.status(404).json({ error: "subtask does not exist" });
      else res.status(200).json({"status": "Updated"});
    }
  );
});

router.delete("/:id", (req, res) => {
  const db = require("../index");

  db.query(
    `DELETE FROM subtasks WHERE id = ${req.params.id}`,
    (err, result) => {
      if (err) res.status(401).json({ error: "Unauthorised" });
      else res.status(200).json({ result: "success" });
    }
  );
});

module.exports = router;