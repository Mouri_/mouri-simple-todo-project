const router = require("express").Router();

router.get("/", (req, res) => {
  const db = require("../index");

  db.query(
    "SELECT tasks.id, users.name, tasks.title, tasks.status FROM tasks JOIN users ON users.id = tasks.name_id ORDER BY id",
    (err, result) => {
      console.log(err);
      if (err) res.status(500).json({ error: "Some error while fetching" });
      else res.status(200).json(result);
    }
  );
});

router.get("/:id", (req, res) => {
  const db = require("../index");

  db.query(
    `SELECT tasks.id, users.name, tasks.title, tasks.status FROM tasks JOIN users ON users.id = tasks.name_id WHERE users.id = ${req.params.id}`,
    (err, result) => {
      if (err) return res.status(404).json({ error: "User does not exist" });
      if(result.length <= 0) return res.status(401).json({ error: "Wrong user id or no tasks" });
      res.status(200).json(result);
    }
  );
});

router.post("/", (req, res) => {
  const db = require("../index");
  const body = req.body;
  db.query(
    `INSERT INTO tasks(name_id, title) VALUES('${body.id}', '${body.title}')`,
    (err, result) => {
      if (err) res.status(409).json({ error: "Task already exists" });
      else res.status(201).json({ result: "Success" });
    }
  );
});

router.put("/:id", (req, res) => {
  const db = require("../index");
  const title = req.body.title || "";
  const status = req.body.status || "";

  const updateValue =
    `id = '${req.params.id}'` +
    `${title && `, title = '${title}'`}` +
    `${status && `, status = '${status}'`}`;

  db.query(
    `UPDATE tasks SET ${updateValue} WHERE id = ${req.params.id} AND name_id = ${req.body.id}`,
    (err, result) => {
      if (err) res.status(404).json({ error: "Task doesnot exist" });
      else res.status(200).json(req.body);
    }
  );
});

router.delete("/:id", (req, res) => {
  const db = require("../index");

  db.query(
    `DELETE FROM tasks WHERE id = ${req.params.id} AND name_id = ${req.body.id}`,
    (err, result) => {
      if (err) res.status(404).json({ error: "Task doesnot exist" });
      else res.status(200).json({ result: "success" });
    }
  );
});

module.exports = router;