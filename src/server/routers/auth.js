const router = require("express").Router();

router.post("/", (req, res) => {
  const bcrypt = require("bcrypt");
  const jwt = require("jsonwebtoken");
  const db = require("../index");
  console.log(req.body)
  const user_name = req.body.user_name;
  const password = req.body.password;

  db.query(
    `SELECT password FROM users WHERE user_name = '${user_name}'`,
    (err, result) => {
      if (err) return res.status(500).json({ error: "Something went wrong" });
      if(result.length <= 0) return res.status(401).json({ error: "Wrong user name" });
      if (bcrypt.compareSync(password, result[0].password)) {
        const token = jwt.sign(user_name, process.env.JWT_SECRET);
        res.status(200).json({ token: token });
      } else {
        res.status(401).json({ error: "Wrong password" });
      }
    }
  );
});

module.exports = router;