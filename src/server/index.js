require("dotenv").config();

const express = require("express");

const jwt = require("jsonwebtoken");

const mysql = require("mysql");

const config = require("./config.json");

const authRoute = require("./routers/auth");

const userRoute = require("./routers/users");

const tasksRoute = require("./routers/tasks");

const subTasksRoute = require("./routers/subTasks");

const dbConfig = config.todoConfig;

const PORT = process.env.PORT || config.PORT;

const db = mysql.createConnection(dbConfig);

const app = express();

db.connect(err => {
  if (err) console.log(`Error while connecting to ${dbConfig.host}`);
  else console.log(`Connection established to ${dbConfig.host}`);
});

app.use(express.json());

app.use("/login", authRoute);

app.use("/", (req, res, next) => {
  if (!req.headers.hasOwnProperty("authorization")) {
    req.body.user = null;
    return next();
  }

  const token = req.headers.authorization.replace("Bearer ", "");
  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) return res.status(401).json({ error: "Invalid token" });
    req.body.user = user;
    db.query(
      `SELECT id FROM users WHERE user_name = '${user}'`,
      (err, result) => {
        if (err)
          return res.status(500).json({ error: "Server error" });
        if (result.length <= 0)
          return res.status(401).json({ error: "Unauthorised (User deleted)" });
        req.body.id = result[0].id;
        next();
      }
    );
  });
});

app.use("/users", userRoute);

app.use("/", (req, res, next) => {
  if (!req.body.user) return res.status(401).json({ error: "Unauthorized" });
  else next();
});

app.use("/tasks", tasksRoute);

app.use("/subtasks", subTasksRoute);

app.use("/", (req, res) => {
  res.status(400).json({ error: "Bad request" });
});

app.listen(PORT, () => console.log("Express server running on port", PORT));

module.exports = db;
