const mysql = require("mysql");

const config = require("./config.json");

const db = mysql.createConnection(config.dbConfig);

const DB_NAME = config.databaseName;

db.connect(err => {
  if (err) console.log(`Error while connecting to ${config.dbConfig.host}`);
  else console.log(`Connection established to ${config.dbConfig.host}`);
});

createDatabase(DB_NAME);

connectToDatabase(DB_NAME);

addTable(
  "users",
  "`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(45) NOT NULL, `user_name` VARCHAR(45) NOT NULL UNIQUE, `email` VARCHAR(45) NOT NULL, `password` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)"
);

addTable(
  "tasks",
  "`id` INT NOT NULL AUTO_INCREMENT, `name_id` INT NOT NULL, `title` VARCHAR(45) NOT NULL UNIQUE, `status` BOOLEAN NOT NULL DEFAULT 0, PRIMARY KEY (`id`, `name_id`), INDEX `fk_tasks_users_idx` (`name_id` ASC) VISIBLE, CONSTRAINT `fk_tasks_users` FOREIGN KEY (`name_id`) REFERENCES `todoDB`.`users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION"
);

addTable(
  "subtasks",
  "`id` INT NOT NULL AUTO_INCREMENT, `task_id` INT NOT NULL, `description` VARCHAR(45) NOT NULL UNIQUE, `status` BOOLEAN NOT NULL DEFAULT 0, PRIMARY KEY (`id`, `task_id`), INDEX `fk_subtasks_tasks_idx` (`task_id` ASC) VISIBLE, CONSTRAINT `fk_subtasks_tasks` FOREIGN KEY (`task_id`) REFERENCES `todoDB`.`tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION"
);

function createDatabase(name) {
  db.query(`CREATE DATABASE ${name}`, (err, result) => {
    if (err) console.log(`Error while creating database ${name}`);
    else console.log(`Created database ${name}`);
  });
}

function addTable(name, columns) {
  db.query(`CREATE TABLE ${name}(${columns})`, (err, result) => {
    if (err) console.log(`Error while adding table ${name}`);
    else console.log(`Added table ${name}`);
  });
}

function connectToDatabase(name) {
  db.changeUser({ database: name }, (err, result) => {
    if (err) console.log(`Error while connecting to ${name}`);
    else console.log(`Connected to ${name}`);
  });
}

db.end();
